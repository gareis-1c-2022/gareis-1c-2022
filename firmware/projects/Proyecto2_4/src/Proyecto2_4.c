/*! @mainpage Proyecto2_4
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		     	| 	            |
 * | 		 	    | 	        	|
 * | 	GND	 	    | 	GND			|
 *      VCC             VCC
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 12/05/2022 | Document creation		                         |
 * |         	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 *Actividad 4 - Proyecto: Osciloscopio
Diseñar e implementar una aplicación, basada en el driver analog_io.h y el driver de transmisión serie uart.h,
que digitalice una señal analógica y la transmita a un graficador de puerto serie de la PC.
Se debe tomar la entrada CH1 del conversor AD y la transmisión se debe realizar por la UART
conectada al puerto serie de la PC, en un formato compatible con un graficador por puerto serie.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_4.h"       /* <= own header */

#include "uart.h"
#include "uart_stdio.h"
#include "analog_io.h"
#include "timer.h"
#include "systemclock.h"

/*==================[macros and definitions]=================================*/

bool timer_flgADC=true;
bool timer_flgECG=true;

#define BUFFER_SIZE 231

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
void Timeradc(void){

	AnalogStartConvertion();

}

void Lee_Muestra(void){

	uint16_t v;

	AnalogInputRead(CH1,&v);

	UartSendString(SERIAL_PORT_PC, Uart_itoa(v, 10));

	UartSendString(SERIAL_PORT_PC, "\r");


}

void Timer_ecg (void){

	static uint8_t j=0;

	if(j>BUFFER_SIZE){

		j=0;

	}else {

		j++;
	}

	AnalogOutputWrite(ecg [j]);

}




int main(void){

	/* initializations */

	SystemClockInit();


	//Timer del ADC

		timer_config Timer_adc={

						TIMER_B,

						2,

						Timeradc

				};

		TimerInit(&Timer_adc);

		TimerStart(Timer_adc.timer);


	// Inicializacion del ADC

	analog_input_config my_input={

				CH1,

				AINPUTS_SINGLE_READ,

				Lee_Muestra
		};

	AnalogInputInit(&my_input);

	AnalogOutputInit();

	//Timer del ECG

	timer_config Timer_ecg={

			TIMER_A,

			4,

			Timer_ecg
	};

	TimerInit(&Timer_ecg);

	TimerStart(Timer_ecg.timer);




	//Inicializacion de Uart

	serial_config my_uart={

			SERIAL_PORT_PC,

			115200,

		NO_INT
    };

    UartInit(&my_uart);






	while(1){
		/* main loop */

	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

