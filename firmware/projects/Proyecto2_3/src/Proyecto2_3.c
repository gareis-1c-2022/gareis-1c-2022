/*! @mainpage Proyecto2_3
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*Actividad 3 - Proyecto: Medidor de distancia por ultrasonido c/interrupciones y puerto serie
Modifique la actividad del punto 2 agregando ahora el puerto serie. Envíe los datos de las mediciones

para poder observarlos en un terminal en la PC, siguiendo el siguiente formato:

3 dígitos ascii + 1 carácter espacio + dos caracteres para la unidad (cm) + cambio de línea “ \r\n”
Además debe ser posible controlar la EDU-CIAA de la siguiente manera:
Con las teclas “O” y “H”, replicar la funcionalidad de las teclas 1 y 2 de la EDU-CIAA
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_3.h"       /* <= own header */
#include "uart.h"
#include "hc_sr04.h"
#include "switch.h"
#include "gpio.h"
#include "lcditse0803.h"
#include "led.h"
#include "timer.h"
#include "systemclock.h"
#include "uart_stdio.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
bool on_off_flag = false;
bool hold_flg = false;
bool timer_flg = true;

typedef struct {
	gpio_t pin; // nro pin
	io_t dir;  // direccion in=0 out=1

} gpioConf_t;

/*==================[internal functions declaration]=========================*/



void MyFuncionSwitch1(void){

	on_off_flag=!on_off_flag;

}

void MyFuncionSwitch2(void){

	hold_flg=!hold_flg;

}

void Timer(void){

	timer_flg=true;

}

void My_uart(void) {

	uint8_t valor;

	UartReadByte(SERIAL_PORT_PC, &valor);

	switch (valor) {

	case 'o':

		on_off_flag = !on_off_flag;

		break;

	case 'h':

		hold_flg = !hold_flg;

		break;

	}

}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

void MostrarValor(distancia){

	LcdItsE0803Write(distancia);

	    	if((distancia>=0)&&(distancia<=10)){

	    		LedOn(LED_RGB_B);

	    		LedOff(LED_1);

	    		LedOff(LED_2);

	    		LedOff(LED_3);

	    	}

	    	if((distancia>10)&&(distancia<=20)){

	    	    		LedOn(LED_RGB_B);

	    	    		LedOn(LED_1);

	    	    		LedOff(LED_2);

	    	    		LedOff(LED_3);

	    	    	}
	    	if((distancia>20)&&(distancia<=30)){

	        	    		LedOn(LED_RGB_B);

	        	    		LedOn(LED_1);

	        	    		LedOn(LED_2);

	        	    		LedOff(LED_3);

	        	    	}

	    	if((distancia>30)){

	    	        	    LedOn(LED_RGB_B);

	    	        	    LedOn(LED_1);

	    	        	    LedOn(LED_2);

	    	        	    LedOn(LED_3);

	    	        	    	}
	    	UartSendString(SERIAL_PORT_PC, Uart_itoa(distancia, 10));

	    	UartSendString(SERIAL_PORT_PC, " cm \r\n");
}

int main(void) {
	/* initializations */

	SystemClockInit();

	SwitchesInit();

	LedsInit();

	HcSr04Init(GPIO_T_FIL0, GPIO_T_FIL2);

	LcdItsE0803Init();

	timer_config my_timer_config = {

	                TIMER_A,

	                1000,
	
	                Timer };

	TimerInit(&my_timer_config);

	TimerStart(my_timer_config.timer);

	serial_config my_uart = {

	                SERIAL_PORT_PC,

	                115200,
	
	                My_uart };

	UartInit(&my_uart);

	uint16_t distancia;

	SwitchActivInt(SWITCH_1, &MyFuncionSwitch1);

	SwitchActivInt(SWITCH_2, &MyFuncionSwitch2);

	while (1) {
		/* main loop */

		if (timer_flg) {

			if (on_off_flag == true) {

				distancia = HcSr04ReadDistanceInCentimeters();

				if (hold_flg == false) {

					MostrarValor(distancia);

				}

			} else {

				//apagar todos los leds

				distancia = 0;

				MostrarValor(distancia);

			}

			timer_flg = false;
		}

	}

	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

