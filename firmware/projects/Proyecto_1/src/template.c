/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */

#include "lcditse0803.h"

#include "hc_sr04.h"

#include "switch.h"

#include "gpio.h"

#include "systemclock.h"

#include "gpio.h"

#include "led.h"

#include "delay.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/



int main(void){
	/* initializations */
	
	SwitchesInit();

	SystemClockInit();
	
	LcdItsE0803Init();

	LedsInit();

	HcSr04Init(GPIO_T_FIL0, GPIO_T_FIL2);


    while(1){
		/* main loop */

    	uint16_t distancia=HcSr04ReadDistanceInCentimeters();

    	LcdItsE0803Write(distancia);

    	if((distancia>=0)&&(distancia<=10)){

    		LedOn(LED_RGB_B);

    		LedOff(LED_1);

    		LedOff(LED_2);

    		LedOff(LED_3);

    	}

    	if((distancia>10)&&(distancia<=20)){

    	    		LedOn(LED_RGB_B);

    	    		LedOn(LED_1);

    	    		LedOff(LED_2);

    	    		LedOff(LED_3);

    	    	}
    	if((distancia>20)&&(distancia<=30)){

        	    		LedOn(LED_RGB_B);

        	    		LedOn(LED_1);

        	    		LedOn(LED_2);

        	    		LedOff(LED_3);

        	    	}

    	if((distancia>30)){

    	        	    LedOn(LED_RGB_B);

    	        	    LedOn(LED_1);

    	        	    LedOn(LED_2);

    	        	    LedOn(LED_3);

    	        	    	}
    	DelaySec(1);

	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

