/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

/*Escriba una función que reciba un dato de 32 bits,
 * la cantidad de dígitos de salida y un puntero a un arreglo donde se almacene los
 * n dígitos.
 * La función deberá convertir el dato recibido a BCD,
 * guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.

 */

void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number ){

	uint32_t i=0;

	while(i<=digits){
		bcd_number[i]= data%10;
		data= data/10;
		i++;
	}


}








int main(void){
	/* initializations */
	
	
    while(1){
		/* main loop */

	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

