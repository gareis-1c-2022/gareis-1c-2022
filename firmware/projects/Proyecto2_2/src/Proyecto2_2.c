/*! @mainpage Proyecto2_2
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_2.h"       /* <= own header */
#include "lcditse0803.h"

#include "hc_sr04.h"

#include "switch.h"

#include "gpio.h"

#include "systemclock.h"

#include "gpio.h"

#include "led.h"

#include "delay.h"

#include "timer.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

/*Actividad 2 - Proyecto: Medidor de distancia por ultrasonido c/interrupciones

Modifique la actividad del punto 1 de manera de utilizar interrupciones para

el control de las teclas y el control de tiempos (Timers). */


void MostrarValor(distancia){

	LcdItsE0803Write(distancia);

	    	if((distancia>=0)&&(distancia<=10)){

	    		LedOn(LED_RGB_B);

	    		LedOff(LED_1);

	    		LedOff(LED_2);

	    		LedOff(LED_3);

	    	}

	    	if((distancia>10)&&(distancia<=20)){

	    	    		LedOn(LED_RGB_B);

	    	    		LedOn(LED_1);

	    	    		LedOff(LED_2);

	    	    		LedOff(LED_3);

	    	    	}
	    	if((distancia>20)&&(distancia<=30)){

	        	    		LedOn(LED_RGB_B);

	        	    		LedOn(LED_1);

	        	    		LedOn(LED_2);

	        	    		LedOff(LED_3);

	        	    	}

	    	if((distancia>30)){

	    	        	    LedOn(LED_RGB_B);

	    	        	    LedOn(LED_1);

	    	        	    LedOn(LED_2);

	    	        	    LedOn(LED_3);

	    	        	    	}
}

void MyFuncionSwitch1(void){

	on_off_flag=!on_off_flag;

}

void MyFuncionSwitch2(void){

	hold_flg=!hold_flg;

}

void Timer(void){

	timer_flg=true;

}

int main(void) {
	/* initializations */

	timer_config my_timer = {
			TIMER_A,
			1000,
			Timer,
	};

	TimerInit(&my_timer);

	TimerStart(my_timer.timer);

	uint16_t distancia;


	SwitchActivInt(SWITCH_1, &MyFuncionSwitch1);

	SwitchActivInt(SWITCH_2,&MyFuncionSwitch2);

    while(1){
		/* main loop */

    	if(timer_flg){

	    	if(on_off_flag==true){

	    		distancia=HcSr04ReadDistanceInCentimeters();

	    		if(hold_flg==false){
	    			MostrarValor(distancia);
	    		}
	    	}
			else{
				distancia=0;
				MostrarValor (distancia);
			}

	    	timer_flg=false;
    	}



	}

	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/
