/*! @mainpage Proyecto2_2
/* Copyright 2019,
 * Electrónica Programable
 * jmrera@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*! @mainpage Proyecto2_1
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 * El programa muestra el valor de distancia en cm utilizando el display LCD, ademas enciende los LEDS seguna las siguientes condiciones:
 *
 *Si la distancia está entre 0 y 10 cm, se enciende el LED_RGB_B (Azul).
 *Si la distancia está entre 10 y 20 cm, se enciende el LED_RGB_B (Azul) y LED_1.
 *Si la distancia está entre 20 y 30 cm, se enciende el LED_RGB_B (Azul), LED_1 y LED_2
 *Si la distancia es mayor a 30 cm, se enciende el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
 *
 *
 *Con la TEC1 se comanda el inicio y fin de la medicion.
 *Con TEC2 se mantiene el resultado por display.
 *Cada 1 s se refresca la medicion.
 *
 *Todas estas acciones estan comandadas por interrupciones.
 *
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Echo	 	| 	GPIO_T_FIL0	|
 * | 	Trigger	 	| 	GPIO_T_FIL2	|
 * | 	GND	 	    | 	GND			|
 *      VCC             VCC
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 07/04/2022 | Document creation		                         |
 * |        	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

#ifndef _PROYECTO2_2_H
#define _PROYECTO2_2_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/** @fn void MostrarValor(uint16_t  distancia)
 * @brief La funcion si la distancia está entre 0 y 10 cm, enciende el LED_RGB_B (Azul).
Si la distancia está entre 10 y 20 cm, enciende el LED_RGB_B (Azul) y LED_1.
Si la distancia está entre 20 y 30 cm, enciende el LED_RGB_B (Azul), LED_1 y LED_2
Si la distancia es mayor a 30 cm, enciende el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
 *
 ** param[uint16_t] distancia, distancia medida por el ultrasonido
 ** @return void
 **/

/** @fn void MyFuncionSwitch1(void)
 * @brief Esta funcion es la interrupcion del switch1, cambiando el estado de la bandera osea de la variable booleana
 *
 ** param[void]
 ** @return void
 **/

/** @fn void MyFuncionSwitch2(void)
 * @brief Esta funcion es la interrupcion del switch2, cambiando el estado de la bandera osea de la variable booleana
 *
 ** param[void]
 ** @return void
 **/

/** @fn void Timer(void)
 * @brief Esta funcion es la interrupcion del timer
 *
 ** param[void]
 ** @return void
 **/

/*==================[end of file]============================================*/



/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

