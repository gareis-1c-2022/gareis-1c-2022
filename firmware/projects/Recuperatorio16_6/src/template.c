/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */

#include "gpio.h"

#include "timer.h"

#include "systemclock.h"

#include "hc_sr04.h"

#include "nivel_tanque.h"

#include "uart.h"

#include "analog_io.h"




/*==================[macros and definitions]=================================*/

bool on_off_flag = false;

float Vol_1, Vol_2;

typedef struct {
	gpio_t pin; // nro pin
	io_t dir;  // direccion in=0 out=1

} gpioConf_t;

analog_input_config HumedadSensor;

gpioConf_t conf1;

conf1.pin=GPIO_3;

conf1.dir=0;

gpioConf_t conf2;

conf2.pin=GPIO_5;

conf2.dir=0;

uint16_t value_humedad;

serial_config my_uart = {

                SERIAL_PORT_PC,

                115200,

                My_uart };

//Timer del ADC

		timer_config Timer_adc={

						TIMER_A,

						60000, // Leo cada un minuto

						FUNCTimeradc

				};




/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

void My_uart(void) {

	uint8_t valor;

	UartReadByte(SERIAL_PORT_PC, &valor);

	switch (valor) {

	case 'I':

		on_off_flag = true;

		break;

	case 'O':

		on_off_flag = false;

		break;

	}

}

void FUNCTimeradc(){

	if(on_off_flag==true){

			nivel_tanqueInit(GPIO_T_FIL0);

			Vol_2=nivel_tanque_litros();

			if((Vol_2<600000.0)&&(Vol_2!=900000.0)){

			   nivel_tanqueInit(GPIO_T_FIL1);

			   Vol_1=nivel_tanque_litros();

					if(Vol_1>200000.0){

						GPIOOn(conf1.pin);

					}

			   }

			else {

			   GPIOOff(conf1.pin);

			   }

			HumedadSensor.input=CH1;

			HumedadSensor.mode=AINPUTS_SINGLE_READ;

			HumedadSensor.pAnalogInput=NULL;

			AnalogInputInit(&HumedadSensor);

			AnalogInputReadPolling(HumedadSensor.input, &value_humedad);

			if(value_humedad<50&&Vol_2>50){

				GPIOOn(conf2.pin);

			}

			else{

				GPIOOff(conf2.pin);

			}

			UartSendString(SERIAL_PORT_PC, " Tanque 1: ");

			UartSendString(SERIAL_PORT_PC, UartItoa(Vol_1, 10));

			UartSendString(SERIAL_PORT_PC, " litros \r\n");

			UartSendString(SERIAL_PORT_PC, " Tanque 2: ");

			UartSendString(SERIAL_PORT_PC, UartItoa(Vol_2, 10));

			UartSendString(SERIAL_PORT_PC, " litros \r\n");

			UartSendString(SERIAL_PORT_PC, " Humedad del suelo: ");

			UartSendString(SERIAL_PORT_PC, UartItoa(value_humedad, 10));

			UartSendString(SERIAL_PORT_PC, " % \r\n");

	}

}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	/* initializations */
	
	SystemClockInit();

	UartInit(&my_uart);
	
	TimerInit(&Timer_adc);

	TimerStart(Timer_adc.timer);

	GPIOInit(conf1.pin, conf1.dir);

	GPIOInit(conf2.pin, conf2.dir);

    while(1){
		/* main loop */

    	if(on_off_flag==false){

    			TimerStop(Timer_adc.timer);

    		}


	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

