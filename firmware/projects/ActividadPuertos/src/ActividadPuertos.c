/*! @mainpage ActividadPuertos
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 *
 *
 * Actividad práctica - Lectura de puertos
En este ejercicio se utiliza la librería gpio.h para comprender el funcionamiento de la lectura de un pin de la placa.
El esquema siguiente (Figura 1) esta basado en el conexionado de la placa EDU-CIAA que debe ser utilizado.
El pin D4 de la placa Arduino (conectado al pin TCOL_1 de la placa EDU-CIAA) genera una señal digital cuadrada de 0.5Hz.

- Para corroborar la señal de la placa Arduino desarrolle un firmware que prenda el LED_2 de la
EDU-CIAA a partir de la lectura continua del pin TCOL_1.  El LED 2 debe encenderse o apagarse una
vez se detecte el flanco ascendente de la señal de reloj mencionada como se observa en la Figura 2.
Utilice los drivers de gpio.h y led.h provistos por la cátedra.

¿Cual es la frecuencia resultante de encendido/apagado del LED?

Entregable: Proyecto comprimido en .zip que resuelva, en la EDU-CIAA,
el comportamiento que observa al cargar el binario en la placa a través del Lab Remoto.

Se sugiere consultar la guía introductoria del laboratorio remoto.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ActividadPuertos.h"       /* <= own header */

#include "led.h"

#include "gpio.h"

#include "systemclock.h"

#include "stdio.h"

#include "stdint.h"


/*==================[macros and definitions]=================================*/

bool EAct_flag=false; 	//Estado Actual.

bool EAnt_flag=false; 	//Estado Anterior.

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	/* initializations */

	SystemClockInit();

	LedsInit();

	GPIOInit(GPIO_T_COL1, GPIO_INPUT);


	LedOff(LED_RGB_R);

	LedOff(LED_RGB_B);

	LedOff(LED_RGB_G);

	LedOff(LED_1);

	LedOff(LED_3);

	
    while(1){

		/* main loop */


    	EAct_flag=GPIORead(GPIO_T_COL1);

    	if(EAct_flag==true && EAnt_flag==false){

    		LedToggle(LED_2);

    		}

    	EAnt_flag=EAct_flag;

	}
    
	/* program should never reach this line */

	return 0;
}

/*==================[end of file]============================================*/


