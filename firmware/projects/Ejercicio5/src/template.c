/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/


#include "gpio.h"

typedef struct
{
	gpio_t pin;				/*!< GPIO pin number */
	io_t dir;				/*!< GPIO direction '0' IN;  '1' OUT */
} gpioConf_t;
void BcdToGpio(gpioConf_t * conf, uint8_t bcd ){
	for(int i=0; i<4; i++){
		GPIOInit(conf[i].pin, conf[i].dir);
	};
    for (int j=0; j<4; j++){
    	GPIOState(conf[j].pin, (bcd&(1<<j))>>j);
    }

}


int main(void){
	/* initializations */
	
	gpioConf_t lcd[4]={{GPIO_LCD_1, GPIO_OUTPUT},
			{GPIO_LCD_2, GPIO_OUTPUT},
			{GPIO_LCD_3, GPIO_OUTPUT},
			{GPIO_LCD_4, GPIO_OUTPUT}};

	BcdToGpio(lcd, 5);
	GPIOInit(GPIO_1, GPIO_OUTPUT);
	GPIOOn(GPIO_1);
	GPIOOff(GPIO_1);
	
    while(1){
		/* main loop */

	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

