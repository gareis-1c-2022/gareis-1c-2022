/*! @mainpage PROYECTO_INTEGRADOR
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Germán Ariel Gareis
 *
 * PROYECTO INTEGRADOR: Dispositivo que transmite los valores de presion de un manguito(esfingomanómetro) a traves de
 * un periferico bluethoot al celular. La aplicacion muestra los valores de presion sistolica y diastolica
 * junto con la grafica de presion en funcion del tiempo.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/PROYECTO_INTEGRADOR.h"       /* <= own header */

#include "stdint.h"
#include "uart.h"
#include "analog_io.h"
#include "timer.h"
#include "systemclock.h"
#include "switch.h"
#include "bool.h"
#include "XFPM_050KPG.h"
#include "chip.h"
#include "sapi_rtc.h"

/*==================[macros and definitions]=================================*/

#define BUFFER_SIZE 5000

uint16_t k=0;

bool on_off_flag=false;

//uint16_t aux=0;

uint16_t min=0;

uint16_t max=0;


/*==================[internal data definition]===============================*/

uint16_t Vpressure[5000]={0};

/*==================[internal functions declaration]=========================*/

/*uint16_t MaxPressure(uint16_t Vector[5000]){



	max=Vector[i];

	i++;

	while(i<5000){

		if(Vector[i]>max){

			max=Vector[i];

		}
		i++;
	}
	return max;
}

uint16_t MinPressure(uint16_t Vector[5000]){



	min=Vector[j];

	j++;

	while(j<5000){

		if(Vector[j]<min){

			min=Vector[j];

		}
		j++;
	}
	return min;
}*/

void FUNCTimeradc(){

	Vpressure[k]=XFPM_050KPG_ReadPressure_mmHg();

    UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*G");

    UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Vpressure[k],10));

	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*");

		if(k<5000){

		k++;

		}

}

/*void FUNCADC(){


	Vpressure[k]=XFPM_050KPG_ReadPressure_mmHg();

	if(k<5000){

	k++;

	}


}*/

void MyFuncionSwitch1(void){

	on_off_flag=true;

}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	

	    //Timer del ADC

		timer_config Timer_adc={

						TIMER_A,

						2, // Uso una frecuencia de muestreo de 500Hz, T=2ms

						FUNCTimeradc

				};

		//Uart

		serial_config my_uart={

				SERIAL_PORT_P2_CONNECTOR,

				115200,

			    NO_INT
	    };


	/* initializations */

	SystemClockInit();

	SwitchesInit();

	SwitchActivInt(SWITCH_1, &MyFuncionSwitch1);

	XFPM_050KPG_Init();

	TimerInit(&Timer_adc);

	UartInit(&my_uart);

		while(1){

		while(on_off_flag==false){

		}

		TimerStart(Timer_adc.timer);


		while(k!=5000){

		}

		TimerStop(Timer_adc.timer);

		min= Vpressure[0];

		max= Vpressure[0];


		for(int i=1; i<5000; i++){

		    if( Vpressure[i]<min){

							min= Vpressure[i];

						}

			if(Vpressure[i]>max){

							max=Vpressure[i];

						}

			}



		UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*T");

		UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(max,10));

		UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*");

		UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*D");

		UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(min,10));

		UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*");



		}
    
	/* program should never reach this line */

	return 0;
}

/*==================[end of file]============================================*/

