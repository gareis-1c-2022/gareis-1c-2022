/*! @mainpage PROYECTO_INTEGRADOR
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |XFPM_050KPG		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	3.3V		|
 * | 	PIN2	 	| 	CH1		    |
 * | 	PIN3	 	| 	GND			|
 *
 *
 * |XFPM_050KPG		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN2	 	| 	5V		    |
 * | 	PIN3	 	| 	RS232_TX    |
 * | 	PIN4	 	| 	RS232_RX	|
 * | 	PIN5	 	| 	GND			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Germán Ariel Gareis
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/


/** @fn void FUNCTimeradc(void)
 * @brief La funcion lee los valores de presion cada 2ms y llena un vector con todos los valores.
 * Ademas envia los valores de presion por bluethoot al celular para que se grafiquen en funcion del tiempo.
 *  ** param[void]
 ** @return void
 *

/** @fn void MyFuncionSwitch1(void)
 * @brief interrupcion de la tecla 1 de la EDU-CIAA. Cuando se presiona la tecta 1 se pone en verdadero una bandera que servira para comandar el incio de la lectura de datos desde el sensor de presion.
 *
 * ** param[void]
 ** @return void


/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

