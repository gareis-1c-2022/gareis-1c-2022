/*! @mainpage Proyecto2_1
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * El programa muestra el valor de distancia en cm utilizando el display LCD, ademas enciende los LEDS seguna las siguientes condiciones:
 *
 *Si la distancia está entre 0 y 10 cm, se enciende el LED_RGB_B (Azul).
 *Si la distancia está entre 10 y 20 cm, se enciende el LED_RGB_B (Azul) y LED_1.
 *Si la distancia está entre 20 y 30 cm, se enciende el LED_RGB_B (Azul), LED_1 y LED_2
 *Si la distancia es mayor a 30 cm, se enciende el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
 *
 *
 *Con la TEC1 se comanda el inicio y fin de la medicion.
 *Con TEC2 se mantiene el resultado por display.
 *Cada 1 s se refresca la medicion.
 *
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_1.h"       /* <= own header */
#include "lcditse0803.h"

#include "hc_sr04.h"

#include "switch.h"

#include "gpio.h"

#include "systemclock.h"

#include "gpio.h"

#include "led.h"

#include "delay.h"

#include "timer.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

void MostrarValor(uint16_t distancia);


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

void MostrarValor(uint16_t  distancia){

	LcdItsE0803Write(distancia);

	    	if((distancia>=0)&&(distancia<=10)){

	    		LedOn(LED_RGB_B);

	    		LedOff(LED_1);

	    		LedOff(LED_2);

	    		LedOff(LED_3);

	    	}

	    	if((distancia>10)&&(distancia<=20)){

	    	    		LedOn(LED_RGB_B);

	    	    		LedOn(LED_1);

	    	    		LedOff(LED_2);

	    	    		LedOff(LED_3);

	    	    	}
	    	if((distancia>20)&&(distancia<=30)){

	        	    		LedOn(LED_RGB_B);

	        	    		LedOn(LED_1);

	        	    		LedOn(LED_2);

	        	    		LedOff(LED_3);

	        	    	}

	    	if((distancia>30)){

	    	        	    LedOn(LED_RGB_B);

	    	        	    LedOn(LED_1);

	    	        	    LedOn(LED_2);

	    	        	    LedOn(LED_3);

	    	        	    	}
}

int main(void) {
	/* initializations */

	SystemClockInit();

	SwitchesInit();

	LcdItsE0803Init();

	LedsInit();

	HcSr04Init(GPIO_T_FIL0, GPIO_T_FIL2);

	bool on_flag = false;

	bool hold_flag = false;

	uint8_t TEC;

	while (1) {
		/* main loop */

		TEC = SwitchesRead();

		DelayMs(200);

		switch (TEC) {

		case SWITCH_1:
			on_flag = !on_flag;
			break;

		case SWITCH_2:
			hold_flag = !hold_flag;
			break;

		}

		if (on_flag) {

			uint16_t distancia = HcSr04ReadDistanceInCentimeters();

			if (!hold_flag) {

				MostrarValor(distancia);
			}

		} else {

			MostrarValor(0);

		}

		DelaySec(1);

	}

	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

