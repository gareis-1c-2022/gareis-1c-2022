/*
 * nivel_tanque.c
 *
 *  Created on: 16 jun. 2022
 *      Author: gagareis
 */

#include "hc_sr04.h"



float Vol_litros;

void nivel_tanqueInit(uint16_t channel){

	HcSr04Init(channel, GPIO_T_FIL2);

	Vol_litros=0;

}

float nivel_tanque_litros(void){

	Vol_litros= ((3.14*95.0*95.0*(140.0- (float)(HcSr04ReadDistanceInCentimeters())))/4)/1000.0;

	return(Vol_litros);

}


