/*
 * VNH3SP30.c
 *
 *  Created on: 21 oct. 2021
 *      Author: Usuario
 */

#include "pwm_sct.h"
#include "gpio.h"
#include "analog_io.h"
#include "lpc_types.h"
#include "VNH3SP30.h"

pwm_out_t _pwmPin = -1;
gpio_t _inaPin = -1;
gpio_t _inbPin = -1;
gpio_t _diagPin = -1;
gpio_t _csPin = -1;
uint8_t forward = true; // last speed command is forward
int speed = 0;

analog_input_config AD_config;


void VNH3SP30_begin(pwm_out_t * pwmPin, gpio_t inaPin, gpio_t inbPin, gpio_t diagPin, int8_t csPin) {
	PWMInit(pwmPin, 1, 20000);
	GPIOInit(inaPin, GPIO_OUTPUT);
	GPIOInit(inbPin, GPIO_OUTPUT);
	GPIOInit(diagPin, GPIO_INPUT);

	GPIOOff(inaPin);
	GPIOOff(inbPin);
	AD_config.input = csPin;
	AD_config.mode = AINPUTS_SINGLE_READ;
	AD_config.pAnalogInput = NULL;
	AnalogInputInit(&AD_config);

	_pwmPin = pwmPin[0];
	_inaPin = inaPin;
	_inbPin = inbPin;
	_diagPin = diagPin;
	_csPin = csPin;

  VNH3SP30_setSpeed(0);
}

uint8_t VNH3SP30_setSpeed(int speed) {
  // Ensure we do not reverse the ina and inb setting in case speed==0 to guarantee the motor
  // will free run to a stop (if you reverse ina and inb setting the controller will issue a full brake)
  if (speed>0 || (speed==0 && forward)) {
	  GPIOOn(_inaPin);
	  GPIOOff(_inbPin);
  	  forward = true;
  } else if (speed<0) {
	  GPIOOff(_inaPin);
	  GPIOOn(_inbPin);
  	speed = - speed;
  	forward = false;
  }
  if (speed>400) speed = 400;
  speed = (forward ? speed : -speed);
  PWMSetDutyCycle(_pwmPin, speed);
  PWMOn();
  return VNH3SP30_status();
}

uint8_t VNH3SP30_brake(int brakePower) {
  if (brakePower<0) brakePower = 0;
  if (brakePower>100) brakePower = 100;
  GPIOOff(_inaPin);
  GPIOOff(_inbPin);
  PWMSetDutyCycle(_pwmPin, brakePower); // map 400 to 255
  PWMOff();
  speed = 0;
  return VNH3SP30_status();
}

uint8_t VNH3SP30_status() {
	return !GPIORead(_diagPin);
}

uint16_t VNH3SP30_motorCurrent() {
	uint16_t buf;
	AnalogInputInit(&AD_config);
	AnalogInputReadPolling(CH1, &buf);
	return buf;
}
