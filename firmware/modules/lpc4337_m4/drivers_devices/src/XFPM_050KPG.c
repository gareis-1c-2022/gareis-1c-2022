/*
 * XFPM_050KPG.c
 *
 *  Created on: 19 may. 2022
 *      Author: gagareis
 */
#include "analog_io.h"
#include "stdint.h"
#include "chip.h"

analog_input_config PressureSensor;

void XFPM_050KPG_Init(void){

	PressureSensor.input=CH1;

	PressureSensor.mode=AINPUTS_SINGLE_READ;

	PressureSensor.pAnalogInput=NULL;

	AnalogInputInit(&PressureSensor);


}

uint16_t XFPM_050KPG_ReadPressure_kPa(void){

	uint16_t value;

	AnalogInputReadPolling(PressureSensor.input, &value);

	return  (uint16_t)((((float)value/5.0)-0.04)/0.018);

}

uint16_t XFPM_050KPG_ReadPressure_mmHg(void){

	uint16_t valuekPa;

	valuekPa=XFPM_050KPG_ReadPressure_kPa();

	return (uint16_t)((float)valuekPa*7.50062);

}

