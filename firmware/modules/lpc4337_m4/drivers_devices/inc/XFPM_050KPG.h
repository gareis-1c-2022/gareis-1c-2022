/*
 * XFPM_050KPG.h
 *
 *  Created on: 19 may. 2022
 *      Author: gagareis
 */

#include <stdint.h>

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_XFPM_050KPG_H_

#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_XFPM_050KPG_H_

/** @fn void XFPM_050KPG_Init(void)
 *  @brief Inicializacion del conversor Analogico digital
 *
 ** param[analog_input_config]
 ** @return void
 **/



void XFPM_050KPG_Init(void);

/** @fn void XFPM_050KPG_ReadPressure_mmHg(void)
 *  @brief Lectura de la presion del manguito mmHg.
 *
 ** param[void]
 ** @return void
 **/

uint16_t XFPM_050KPG_ReadPressure_kPa(void);

/** @fn void XFPM_050KPG_ReadPressure_kPa(void)
 *  @brief Lectura de la presion del manguito en kPa.
 *
 ** param[void]
 ** @return void
 **/

uint16_t XFPM_050KPG_ReadPressure_mmHg(void);

#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_XFPM_050KPG_H_ */
