/*
 * nivel_tanque.h
 *
 *  Created on: 16 jun. 2022
 *      Author: gagareis
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_NIVEL_TANQUE_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_NIVEL_TANQUE_H_

void nivel_tanqueInit(uint16_t channel);

float nivel_tanque_litros(void);

#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_NIVEL_TANQUE_H_ */
